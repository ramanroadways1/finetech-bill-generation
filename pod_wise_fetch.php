<?php
 
	require('connect.php');
 
    $report = $conn -> real_escape_string($_POST['seltype']);
    if(isset($_POST['veh'])){
   	 	$branch = $conn -> real_escape_string($_POST['veh']);
	} else {
		$branch = "";
	}
	if(isset($_POST['daterange'])){
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
	} else {
		$daterange = "";
	}
	if(isset($_POST['bno'])){
    	$lrno = $conn -> real_escape_string($_POST['bno']);
	} else {
		$lrno = "";
	}
  
	function before ($thiss, $inthat)
	{
	    return substr($inthat, 0, strpos($inthat, $thiss));
	}

	function after ($thiss, $inthat)
	{
	    if (!is_bool(strpos($inthat, $thiss)))
	        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
	}
	 
	$fromdate = before('-', $daterange); 
	$fromdate = strtotime($fromdate); 
	$todate = after('-', $daterange); 
	$todate = strtotime($todate);

		echo '
		<div class="card-body" style="margin-bottom: 30px;"> 
		<form action="" id="genbill" method="POST" autocomplete="off">  
		<button type="submit" class="btn btn-primary btn-sm btn-icon-split" style="float: right !important; margin: 0px; padding-top:8px; padding-bottom: 8px;">
		<span class="icon text-white-50">
		<i class="fa fa-download"></i> &nbsp;
		</span>
		<span class="text" style="font-weight: bold; letter-spacing: 0.5px;">Create Bill</span>
		</button> 
		 <input placeholder="Bill No" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9 /-]/,\'\')" style="float: right !important; width: 150px;  margin-left:10px; margin-right: 10px;" type="text" name="bill" class="form-control" required=""> 
		</form>  

		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>Sno</th>
					<th>FM No</th>
					<th>LR No</th>
					<th>LR Date</th>
					<th>LR Branch</th>
					<th>Destination</th>
					<th>Material</th>
					<th>TruckNo</th>
					<th>Consignee</th>
					<th>Unloading</th>

					<th>Upload</th>  
					<th width="200px;">Action</th>  
					<th>POD Date</th>
					<th>POD Branch</th>
					<th>#</th>
									</tr>
		      </thead> 
		 	</table>
		</div>
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 100, 500, -1], [10, 100, 500, "All"] ], 
		"bProcessing": true,
  		"searchDelay": 1000,
        "scrollY": 450,
        "scrollX": true,
		"sAjaxSource": "pod_wise_data.php?p='.$branch.'&f='.$fromdate.'&t='.$todate.'&r='.$report.'&l='.$lrno.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"ordering": false, 
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
        },
		dom: "<\'toolbar\'>lBfrtip",
		"buttons": [
		// "copy", "csv", "excel", "print"
		],
		//"order": [[ 8, "desc" ]],
		"columnDefs":[
		{
		// "targets":[4],
		// "orderable":false,
		},
		], 
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  
 
     			$("div.toolbar").html(\'<select class="form-control" style="width: 180px; max-height:32px; float:right;" id="table-filter"><option value="">All LR</option><option value="Checked">Checked LR</option> </select>\'); 
		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		$("#table-filter").on("change", function(){
		table.search(this.value).draw();   
		});
		} );  

		</script>';
