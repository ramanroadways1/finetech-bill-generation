<?php

require('connect.php');
 
  if(isset($_REQUEST['p'])){
    $branch = $conn -> real_escape_string($_REQUEST['p']);
  } else {
    $branch = "";
  }
  
  if(isset($_REQUEST['f'])){
    $fromdate = $conn -> real_escape_string($_REQUEST['f']);
  } else {
    $fromdate = "";
  }
  
  if(isset($_REQUEST['t'])){
    $todate = $conn -> real_escape_string($_REQUEST['t']);
  } else {
    $todate = "";
  }

  if(isset($_REQUEST['r'])){
    $report = $conn -> real_escape_string($_REQUEST['r']);
  } else {
    $report = "";
  }

  if(isset($_REQUEST['l'])){
    $lrno = $conn -> real_escape_string($_REQUEST['l']);
  } else {
    $lrno = "";
  }


 

  $DATABASE_rrpl = "rrpl_database";

	$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );

if($report=="report3"){
    $statement = $connection->prepare("select r.ho_pod_check_time, r.ho_pod_check, r.reupload, l.truck_no, r.del_date,r.veh_type,l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rcv_pod r LEFT join lr_sample l on l.lrno = r.lrno where l.lrno='$lrno' order by r.lrno");

} else {

    $fromdate = date("Y-m-d",$fromdate);
    $todate = date("Y-m-d",$todate);

    $sqli = "select * from diesel_api.dsl_cpanel where company='FINETECH' and title='approval'";
    $resi = $conn->query($sqli);
    $rowi = $resi->fetch_assoc();

    if($rowi['value']=="1"){
        
        if($branch=="ALL"){
        $statement = $connection->prepare("select r.ho_pod_check_time, r.ho_pod_check, r.reupload, l.truck_no, r.del_date,r.veh_type, l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rcv_pod r LEFT join lr_sample l on l.lrno = r.lrno where r.ho_pod_check='1' and r.consignor_id='56' and (l.date BETWEEN '$fromdate' and '$todate') and r.bill_no='' order by l.date, r.lrno");

        } else {

        $statement = $connection->prepare("select r.ho_pod_check_time, r.ho_pod_check, r.reupload, l.truck_no,  r.del_date,r.veh_type, l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rcv_pod r LEFT join lr_sample l on l.lrno = r.lrno where r.ho_pod_check='1' and r.consignor_id='56' and l.branch='$branch' and (l.date BETWEEN '$fromdate' and '$todate') and r.bill_no='' order by l.date, r.lrno");
        }

    } else {
        
        if($branch=="ALL"){
        $statement = $connection->prepare("select r.ho_pod_check_time, r.ho_pod_check, r.reupload, l.truck_no,  r.del_date,r.veh_type, l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rcv_pod r LEFT join lr_sample l on l.lrno = r.lrno where r.consignor_id='56' and (l.date BETWEEN '$fromdate' and '$todate') and r.bill_no='' order by l.date, r.lrno");

        } else {

        $statement = $connection->prepare("select r.ho_pod_check_time, r.ho_pod_check, r.reupload, l.truck_no, r.del_date,r.veh_type, l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rcv_pod r LEFT join lr_sample l on l.lrno = r.lrno where r.consignor_id='56' and l.branch='$branch' and (l.date BETWEEN '$fromdate' and '$todate') and r.bill_no='' order by l.date, r.lrno");
        }
    } 
}


	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();
$sno="0";
foreach($result as $row)
{ 
  $sno++;
	$sub_array = array(); 

  $sub_array[] = $sno;
  $sub_array[] = $row["fno"];
  $sub_array[] = $row["lrno"];
  $sub_array[] = date('d/m/Y', strtotime($row['lrdate']));
  $sub_array[] = $row["lrbranch"];
  $sub_array[] = $row["desti"];
  $sub_array[] = $row["item"];
  $sub_array[] = $row["truck_no"];
  $sub_array[] = $row["consignee"];
  $sub_array[] = date('d/m/Y', strtotime($row['del_date']));


  $pod_files1 = array(); 
  $copy_no = 0;
  foreach(explode(",",$row['upload']) as $pod_copies)
  {
  $copy_no++;
        if (strpos($pod_copies, 'pdf') !== false) {
        $file = 'PDF';
        } else {
        $file = 'IMAGE';
        }
  if($row['veh_type']=="MARKET"){
    $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
   } else {
    $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
   }
  }
  
  $sub_array[] = implode(", ",$pod_files1);
    
   // if(strrchr($row['upload'],".pdf")!=".pdf"){
   //  $btn = "<center> NA </center>";
   // } else {
if($row['bill_no']==""){

      if($row['bill_done']==1)
      {
        $btn= "
        <center><b><font color='green'>Checked </font></b> &nbsp; &nbsp; 
        <button onclick='action(".$row['id'].", this.id)' id='reject' class='btn btn-sm btn-danger' style='font-size:10px !important;  letter-spacing: 1px'>  <b>UNSELECT</b> </button></center>";
      }
      else
      {
        $btn= "
         <center><button onclick='action(".$row['id'].", this.id)' id='select' class='btn btn-sm btn-success' style='font-size:10px !important; letter-spacing: 1px;'> <b>SELECT</b> </button>
         &nbsp; &nbsp; 
       <b><font color='red'>Unselected</font></b></center> ";
      }   
} else {
  $btn = "Bill Generated No: ".$row['bill_no'];
}
   // }

      
	$sub_array[] = " $btn "; 
  $sub_array[] = date('d/m/Y', strtotime($row['poddate']));
  $sub_array[] = $row["podbranch"];
  if($row["ho_pod_check"]=='-1'){
      $sub_array[] = "<center><button onclick='rejectpod(".$row['id'].", this.id)' id='' class='btn btn-sm btn-default' style='font-size:12px !important;  letter-spacing: 1px'>  <b>X </b> </button></center>";

  }else {
      $sub_array[] = "<center><button onclick='rejectpod(".$row['id'].", this.id)' id='' class='btn btn-sm btn-warning' style='font-size:12px !important;  letter-spacing: 1px'>  <b>X </b> </button></center>";
  }
	$data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 