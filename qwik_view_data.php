<?php

require('connect.php');

    $a = $conn -> real_escape_string($_REQUEST['a']); 
    $b = $conn -> real_escape_string($_REQUEST['b']); 
	$b = str_replace('_', '=', base64_decode($b));

	$connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );

	 if($a=="LR"){
		$statement = $connection->prepare("select r.del_date,r.veh_type,l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rcv_pod r LEFT join lr_sample l on l.lrno = r.lrno where r.lrno like '%$b%' order by r.lrno");
	 } else if($a=="BILL"){
		$statement = $connection->prepare("select r.del_date,r.veh_type,l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rcv_pod r LEFT join lr_sample l on l.lrno = r.lrno where r.bill_no like '%$b%' order by r.lrno");
	 }
 
	$statement->execute();
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	$data = array();

foreach($result as $row)
{

  $sub_array = array();  
	
  $sub_array[] = $row["fno"];
  $sub_array[] = $row["lrno"];
  $sub_array[] = date('d/m/Y', strtotime($row['lrdate']));
  $sub_array[] = $row["lrbranch"];
  $sub_array[] = $row["desti"];
  $sub_array[] = $row["item"];
  $sub_array[] = $row["consignee"];
  $sub_array[] = date('d/m/Y', strtotime($row['del_date']));
  $sub_array[] = date('d/m/Y', strtotime($row['poddate']));
  $sub_array[] = $row["podbranch"];

  $pod_files1 = array(); 
  $copy_no = 0;
  foreach(explode(",",$row['upload']) as $pod_copies)
  {
  $copy_no++;
        if (strpos($pod_copies, 'pdf') !== false) {
        $file = 'PDF';
        } else {
        $file = 'IMAGE';
        }
  if($row['veh_type']=="MARKET"){
    $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
   } else {
    $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
   }
  }
  
  $sub_array[] = implode(", ",$pod_files1);

  if($a=="BILL"){
      $sub_array[] = $row["bill_no"]." &nbsp;  <button onclick='delete_req(".$row['id'].")' class='btn btn-sm btn-danger' >  <i class='fa fa-times-circle'></i>  </button> ";
  } else {
      $sub_array[] = $row["bill_no"];
  }

  $data[] = $sub_array;

} 

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>