<?php require('connect.php'); ?>

<!DOCTYPE html>
<html lang="en"> 
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../pod_portal/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../pod_portal/assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
  RAMAN ROADWAYS Pvt. Ltd.
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="../../pod_portal/assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../../pod_portal/assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../../pod_portal/assets/demo/demo.css" rel="stylesheet" />

  <script src="../../pod_portal/assets/js/core/jquery.min.js"></script>
  <script src="../../pod_portal/assets/js/core/bootstrap.min.js"></script>

  <link rel="stylesheet" href="../../pod_portal/assets/jquery-ui.min.css" type="text/css" />     
  <script type="text/javascript" src="../../pod_portal/assets/jquery-ui.min.js"></script>  
  <script src="../../pod_portal/assets/jquery.dataTables.min.js"></script>  

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<style type="text/css">
  input[required], select[required] {
    background-image: url('../../pod_portal/assets/qD0jR.png');
    background-repeat: no-repeat;
    background-position-x: right;
  }
  input{
    text-transform: uppercase;
  }
  label{color: #5a5a5a;}
</style>

<body class="" >
 
 <script type="text/javascript">
 </script>
  <div id="loadicon" style="display:none; position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#ffffff; z-index: 30001; opacity:0.8; cursor: wait;">
  <center><img src="../../pod_portal/assets/loader.gif" style="margin-top:50px;" /> </center>
  </div>

  <div class="wrapper "> 
    <style type="text/css">
      .main-panel{
        width: calc(100%) !important;
      }

      .navbar.navbar-transparent{
        background-color: #fff !important;
      }
    </style>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent" >
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
			<a href="../"><button class="btn btn-danger btn-sm">Go Back</button></a> &nbsp; &nbsp;
            <a class="navbar-brand" href="index.php">
            <i class="fa fa-truck" aria-hidden="true"></i> RAMAN_ROADWAYS
            </a>


          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
             
            <ul class="navbar-nav"> 
   
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-cog" aria-hidden="true"></i> ADMIN 
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
				<!--<a class="dropdown-item" href="adm_pen_index.php"> CREATE BILL  </a>  
                  <a class="dropdown-item" href="adm_bill_index.php"> VIEW POD/BILL  </a>-->
					<a class="dropdown-item" href="qwik_index.php"> CREATE BILL  </a>  
					<a class="dropdown-item" href="qwik_view_index.php"> VIEW POD/BILL  </a>	
					<a class="dropdown-item" href="qwik_down.php"> LR Download  </a>
					<a class="dropdown-item" href="qwik_upload.php"> Upload LR  </a>		
<a class="dropdown-item" href="pod_wise_index.php"> CREATE BILL (POD DATE)  </a>	
<a class="dropdown-item" href="pod_wise_down.php"> LR Download  (POD DATE)  </a>
                  <!-- <a class="dropdown-item" href="logout.php"> LOG OUT </a>   -->
                </div>
              </li>  
 
            </ul>
          </div>
        </div>
      </nav> 
      <div class="content" >
        