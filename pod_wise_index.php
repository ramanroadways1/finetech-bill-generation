<?php require('header.php'); 
?>

  
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<style type="text/css">
  .dataTables_scroll{
    margin-bottom: 20px;
  }
  .dataTables_paginate{
    background: #fff;
  }
  table{
    margin: 0px !important;
  }
  .applyBtn{
    border-radius: 0px !important;
  }
  .show-calendar{
    top: 180px !important;
  } 
    .applyBtn{
        border-radius: 0px !important;
    }
    table.table-bordered.dataTable td {
        padding: 10px 5px 10px 10px;
    }
     .dt-buttons{float: right;}
    .user_data_filter{
        float: right;
    }

    .dt-button {
        padding: 5px 20px;
        text-transform: uppercase;
        font-size: 12px;
        text-align: center;
        cursor: pointer;
        outline: none;
        color: #fff;
        background-color: #37474f ;
        border: none;
        border-radius:  2px;
        box-shadow: 0 4px #999;
    }

    .dt-button:hover {background-color: #3e8e41}

    .dt-button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    #user_data_wrapper{
        width: 100% !important;
    }
    .dt-buttons{
        margin-bottom: 20px;
    }


#appenddiv, #appenddiv2 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444;} .component{display: none;} 
  table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
  .table .thead-light th{
    text-transform: none !important;
  }
</style>
<script type="text/javascript"> 

  $(function() {
    $("#aftername").autocomplete({
    source: 'qwik_auto.php',
    appendTo: '#appenddiv2',
    select: function (event, ui) { 
               $('#aftername').val(ui.item.value);   
               $('#afterid').val(ui.item.dbid);      
               return false;
    },
    change: function (event, ui) {
    if(!ui.item){
        $(event.target).val("");
      Swal.fire({
      icon: 'error',
      title: 'Error !!!',
      text: 'LRNo does not exists !'
      })
      $("#afterid").val("");
      $("#aftername").val("");
      $("#aftername").focus();
    }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
  });

</script>
 <div class="col-md-12"> <h3> CLUB POD <font color=orange> (POD DATE WISE) </font> </h3> </div>
  
 <div id="response"></div>
  
<form method="post" action="" id="getPAGE" autocomplete="off"> 
<div class="col-md-12" >
<div class="card-body" style="background-color: #fff; border: 1px solid #ccc;">
  <div class="row">
  
  <div class="col-md-2 ">
      <label style="text-transform: uppercase;"> TYPE </label>
      <select id="seltype" class="form-control" name="seltype"  onChange="updt(this);" required="required">
        <option value=""> -- select -- </option>
        <option value="report5"> Branch Wise </option> 
        <option value="report3"> LR no Wise </option> 
      </select>
    </div>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style type="text/css">
      .show-calendar{
        top: 250px !important;
      }
    </style>
    
   
    <div class="col-md-2">
      <label style="text-transform: uppercase;"> LR BRANCH </label>
      <select class="form-control" name="veh" id="veh"  disabled="">

        <option value=""> -- select -- </option>
        <option value="ALL"> ALL BRANCH </option>
        <?php
           $sql = "select username from rrpl_database.user where role='2' AND username not in('HEAD','DUMMY') order by username asc";
          $res = $conn->query($sql);
          while($roo = $res->fetch_assoc()){
            echo "<option value='".$roo['username']."'> ".$roo['username']." </option>";
          } 
        ?> 
      </select>
    </div>

    <div class="col-md-3">
      <label style="text-transform: uppercase;">POD DATE RANGE</label>
      <input type="text" name="daterange" class="form-control" value="" disabled="" id="dt" />
      <input type="hidden" id="fromdate" name="fromdate">
      <input type="hidden" id="todate" name="todate"> 
    </div>
  
  <div class="col-md-2">
            <label> LR No </label>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="text" class="form-control" id="aftername" name="bno" value="" style="font-size: 18px; text-transform: uppercase;" disabled="" />
            <div id="appenddiv2"></div>
            <input oninput="this.value=this.value.replace(/[^a-z 0-9 A-Z.&-]/,'')" type="hidden" id="afterid" value="" name="bid">
    </div> 
 
<script type="text/javascript">
  $(function() {
      $('input[name="daterange"]').daterangepicker({
        // minDate: '2019-09-15',
        opens: 'right'
      }, function(start, end, label) {
        document.getElementById('fromdate').value=start.format('YYYY-MM-DD');
        document.getElementById('todate').value=end.format('YYYY-MM-DD'); 
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
      });
  });
</script>

    <div class="col-md-1" style="top:-5px !important;"> 
      <label style=""> </label> <br>
      <button type="submit" class='btn btn-success'> <i class='fa fa-search '></i> <b>  </b> </button>    
      
    </div>
  </div>
</div>
</form> 
</div>

<div id="getPAGEDIV" class="col-md-12" style="margin-top: 25px !important;"></div>

 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
 </div>

<script type="text/javascript">
  function updt(sel) { 
 
       
        if(sel.value == "report3") 
        {
          $("#aftername").prop('required',true);
          $("#aftername").prop('disabled',false);

        $("#dt").prop('required',false);
        $("#dt").prop('disabled',true);
        } else {
          $("#aftername").prop('required',false);
          $("#aftername").prop('disabled',true);

        }

        
        if(sel.value == "report5")
      {
        $("#veh").prop('required',true);
        $("#veh").prop('disabled',false);        
        $("#dt").prop('required',true);
        $("#dt").prop('disabled',false);
      } else {
        $("#veh").prop('required',false);
        $("#veh").prop('disabled',true);

        $("#dt").prop('required',false);
        $("#dt").prop('disabled',true); 
      }
  } 
 
  $(document).on('submit', '#getPAGE', function()
    {   
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'pod_wise_fetch.php',
        data : data,
        success: function(data) {       
        $('#getPAGEDIV').html(data);  
        }
      });
      return false;  
   });

  function action(val,stat){
        $('#loadicon').show();
        $.ajax({
            type: 'POST',
            url: 'adm_pen_action.php',
            data: {id: val, act: stat},
            success: function(data){ 
            // alert(data);
            $('#user_data').DataTable().ajax.reload(null, false); 
            $('#loadicon').hide(); 
            }
        }); 
  }


    function rejectpod(val,stat){
    $('#loadicon').show(); 
    var val = val;  
    var stat = stat;  
           $.ajax({  
                url:"pod_reject_modal.php",  
                method:"post",  
                data: {id: val, act: stat},
                success:function(data){  
                     $('#modaldetail').html(data);  
                     $('#exampleModal').modal("show");  
                     $('#loadicon').hide(); 
                }  
           });
    }

    $(document).on('submit', '#rform', function()
    {  
    $('#loadicon').show(); 
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'pod_reject_save.php',
        data : data,
        success: function(data) {  
        $('#exampleModal').modal("hide");  
        $('#response').html(data);  
        $('#user_data').DataTable().ajax.reload(null, false);  
        $('#loadicon').hide(); 
        }
      });
      return false;
    });

  //   function rejectpod(val,stat){
  //     if(confirm("Are u sure to Reject POD ?")){

  //       $('#loadicon').show();
  //       $.ajax({
  //           type: 'POST',
  //           url: 'pod_reject_save.php',
  //           data: {id: val, act: stat},
  //           success: function(data){ 
  //           $('#response').html(data);  
  //           $('#user_data').DataTable().ajax.reload(null, false); 
  //           $('#loadicon').hide(); 
  //           }
  //       }); 

  //     } else {

  //     }
  // }

  $(document).ready(function()
  { 
    $(document).on('submit', '#genbill', function()
    {  
              $('#loadicon').show();

      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'adm_pen_save.php',
        data : data,
        success: function(data) {  
                     $('#response').html(data);  
        // document.getElementById("hidemodal").click();  
        // window.alert(data);
        document.getElementById("genbill").reset();
            $('#loadicon').hide(); 

            $('#user_data').DataTable().ajax.reload(null, false); 
        }
      });
      return false;
    }); 
   }); 
</script>
 <?php include('footer.php'); ?>