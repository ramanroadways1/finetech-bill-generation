  <?php
   require('connect.php');

    $type = $conn -> real_escape_string($_POST['type']); 
    @$lrno = $conn -> real_escape_string($_POST['lrno']); 
    @$billno = $conn -> real_escape_string($_POST['billno']); 
 
    $lrno = str_replace('=', '_', base64_encode($lrno)); 
    $billno = str_replace('=', '_', base64_encode($billno));  
 
 $a = $type;
 $b = "";
 if($a=="LR"){
  $b = $lrno;
 } else if($a=="BILL"){
  $b = $billno;
 }

?>

  <table id="user_data" class="table table-bordered table-hover">
      <thead class="thead-light">
        <tr>
          <th>FM No</th>
          <th>LR No</th>
          <th>LR Date</th>
          <th>LR Branch</th>
          <th>Destination</th>
          <th>Material</th>
          <th>Consignee</th>
          <th>Unloading</th>
          <th>POD Date</th>
          <th>POD Branch</th>
          <th>Upload</th>  
          <th>BillNo</th>  
        </tr>
      </thead>
  
  </table>

  <script>
  jQuery( document ).ready(function() {

$('#loadicon').show(); 
var table = jQuery('#user_data').dataTable({
     "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
"buttons": [
    "copy", "csv", "excel", "print"
    ],
     "bProcessing": true,
     "sAjaxSource": "qwik_view_data.php?a=<?php echo $a; ?>&b=<?php echo $b; ?>",
      "bPaginate": false,
            "dom": '<"toolbar">Bfrtip',

      "sPaginationType":"full_numbers",
      "iDisplayLength": 10,
      "order": [[ 5, "asc" ]],
      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10],
        "orderable":false,
      },
      ],
      "aoColumns": [
        { mData: '0' },
        { mData: '1' } ,
        { mData: '2' },
        { mData: '3' },
        { mData: '4' },
        { mData: '5' },
        { mData: '6' },
        { mData: '7' },
        { mData: '8' },
        { mData: '9' },
        { mData: '10' },
        { mData: '11' }
    ],
    "initComplete": function( settings, json ) {
    $('#loadicon').hide();
    } 
});  

<?php
if($a=="BILL"){
 ?>
        $("div.toolbar").html('<a style="text-decoration: none; color:red;" target="_blank" href="adm_pen_view.php?id=<?php echo $b; ?>">  Download System Generated Bill </a>');
<?php
 }
 ?>
}); 
</script>