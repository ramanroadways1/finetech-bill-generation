<?php

    require('connect.php');
 
    $p = $conn->real_escape_string($_REQUEST['p']); 
   
    $connection = new PDO('mysql:host='.$DATABASE_HOST.';dbname='.$DATABASE_rrpl.';', $DATABASE_USER, $DATABASE_PASS );
    $statement = $connection->prepare("SELECT r.del_date, r.veh_type, f.id, f.billno, f.lrno, f.status, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti, l.item as item, l.consignee, r.pod_date as poddate, r.branch as podbranch, r.pod_copy as upload, r.bill_no as billstatus FROM finetech_lr f left join lr_sample l on l.lrno = f.lrno left join rcv_pod r on r.lrno = f.lrno WHERE f.uploadid='$p' order by r.lrno");
 
$statement->execute();
$result = $statement->fetchAll();
$count = $statement->rowCount();
$data = array();

$sno=0;
foreach($result as $row)
{ 
    $flag = '0';

    $sno = $sno+1;
	$sub_array = array(); 
    
    $sub_array[] = $row["lrno"]; 
    $sub_array[] = $row["billno"]; 

    if($row["lrdate"]!=NULL){
    $sub_array[] = date('d/m/Y', strtotime($row['lrdate']));      
    } else {
    $sub_array[] = "<font color='red'>LR not found</font>";  
    $flag = '1';    
    }  

    if($row["lrbranch"]!=NULL){
    $sub_array[] = $row["lrbranch"];      
    } else {
    $sub_array[] = "<font color='red'>LR not found</font>"; 
    $flag = '1';    
    }  

    if($row["desti"]!=NULL){
    $sub_array[] = $row["desti"];      
    } else {
    $sub_array[] = "<font color='red'>LR not found</font>";
    $flag = '1';    
    }  

    if($row["item"]!=NULL){
    $sub_array[] = $row["item"];      
    } else {
    $sub_array[] = "<font color='red'>LR not found</font>";      
    $flag = '1';    
    }  

    if($row["consignee"]!=NULL){
    $sub_array[] = $row["consignee"];      
    } else {
    $sub_array[] = "<font color='red'>LR not found</font>";     
    $flag = '1';    
    }  

    if($row["del_date"]!=NULL){
    $sub_array[] = date('d/m/Y', strtotime($row['del_date']));    
    } else {
    $sub_array[] = "<font color='red'>LR not found</font>";     
    $flag = '1';    
    }  

    if($row["billstatus"]!=""){
    $sub_array[] = "<font color='red'>".$row["billstatus"]."</font>";
    $flag = '1';    
    } else {
    $sub_array[] = "Not billed";      
    }

    if($row["poddate"]!=NULL){
    $sub_array[] = date('d/m/Y', strtotime($row['poddate'])); 
    } else {
    $sub_array[] = "<font color='red'>POD not received</font>";    
    $flag = '1';    
    }  

    if($row["podbranch"]!=NULL){
    $sub_array[] = $row["podbranch"];      
    } else {
    $sub_array[] = "<font color='red'>POD not received</font>";   
    $flag = '1';    
    }  

 
      $pod_files1 = array(); 
      $copy_no = 0;
  foreach(explode(",",$row['upload']) as $pod_copies)
  {
  $copy_no++;
        if (strpos($pod_copies, 'pdf') !== false) {
        $file = 'PDF';
        } else {
        $file = 'IMAGE';
        }
  if($row['veh_type']=="MARKET"){
    $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
   } else {
    $pod_files1[] = "<a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a>";
   }
  }

    if($row["upload"]!=NULL){
    $sub_array[] = implode(", ",$pod_files1);    
    } else {
    $sub_array[] = "<font color='red'>POD not received</font>";      
    $flag = '1';    
    }  
  
    $sql = $conn->query("update rrpl_database.finetech_lr set status='$flag' where id='$row[id]'");

    if($row['status']=="0" && $flag=="0"){
        $fstat = "<font color='green'> OK </font>";
    } else {
        $fstat = "<font color='red'> Invalid </font>";
    } 

    $sub_array[] = $fstat;      

 
	$data[] = $sub_array;

}  

$results = array(
	"sEcho" => 1,
    "iTotalRecords" => $count,
    "iTotalDisplayRecords" => $count,
    "aaData"=>$data);

echo json_encode($results); 
exit
?>
 