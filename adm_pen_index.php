<?php 
  include_once 'header.php';
?>  
<style> 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-left:10px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
  padding: 8px;
}
</style>

<div id="response"></div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
</div>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">

<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 

<div class="col-md-12" >
<div class="card shadow mb-4" style="border-radius: 0px !important;"> 
<div class="card-header">
 
<h6 class="m-0 font-weight-bold text-dark" style="top: 6px; float: left !important; font-family: Verdana, Geneva, sans-serif; font-weight: normal; text-transform: uppercase;"> GENERATE BILL (Club POD)  </h6>
 
<form action="" id="genbill" method="POST" autocomplete="off">  
<button type="submit" class="btn btn-primary btn-sm btn-icon-split" style="float: right !important; margin: 0px; padding-top:8px; padding-bottom: 8px;">
<span class="icon text-white-50">
<i class="fa fa-download"></i> &nbsp;
</span>
<span class="text" style="font-weight: bold; letter-spacing: 0.5px;">Create Bill</span>
</button> 
 <input placeholder="Bill No" oninput="this.value=this.value.replace(/[^a-z A-Z,0-9 /-]/,'')" style="float: right !important; width: 150px; margin-right: 10px;" type="text" name="bill" class="form-control" required=""> 

</form>  



</div>
<div class="card-body table-responsive" style="margin-top: 10px;">

  <table id="user_data" class="table table-bordered table-hover" style="">
      <thead class="thead-light">
        <tr>
        <th style=" text-align: center; font-size: 11px; color:#444;"> FM No </th>
        <th style=" text-align: center; font-size: 11px; color:#444;"> LR No </th>
        <th style=" text-align: center; font-size: 11px; color:#444;"> Branch </th>
        <th style=" text-align: center; font-size: 11px; color:#444;"> FM Date </th>
        <th style=" text-align: center; font-size: 11px; color:#444;"> POD_Branch </th>
        <th style=" text-align: center; font-size: 11px; color:#444;"> POD Date </th>
        <th style=" text-align: center; font-size: 11px; color:#444;"> Amount </th>
        <th style=" text-align: center; font-size: 11px; color:#444;"> Front </th>
        <th style=" text-align: center; font-size: 11px; color:#444;"> Rear </th>
        <th style=" text-align: center; font-size: 11px; color:#444; width: 200px;"> Action </th> 
        </tr>
      </thead>
  
  </table>
</div>
</div>
</div> 
</div>
</div> 
</div>
</div>


<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content" id="employee_detail">   
           </div>  
      </div>  
</div>    

<script type="text/javascript">
jQuery( document ).ready(function() {

$('#loadicon').show(); 
var table = jQuery('#user_data').dataTable({
     "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
     "bProcessing": true,
     "sAjaxSource": "adm_pen_fetch.php",
      "bPaginate": true,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 10,
      //"order": [[ 8, "desc" ]],
      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        "orderable":false,
      },
      ],
      "aoColumns": [
        { mData: '0' },
        { mData: '1' } ,
        { mData: '2' },
        { mData: '3' },
        { mData: '4' },
        { mData: '5' },
        { mData: '6' },
        { mData: '7' },
        { mData: '8' },
        { mData: '9' }
    ],
    "initComplete": function( settings, json ) {
    $('#loadicon').hide();
    }
});  
   
}); 

  function action(val,stat){
        $('#loadicon').show();
        $.ajax({
            type: 'POST',
            url: 'adm_pen_action.php',
            data: {id: val, act: stat},
            success: function(data){ 
            // alert(data);
            $('#user_data').DataTable().ajax.reload();
            $('#loadicon').hide(); 
            }
        }); 
  }

</script>
<script type="text/javascript">
$(document).ready(function() { 
    var table = $('#user_data').DataTable(); 
} );
</script>  

<script> 
  $(document).ready(function()
  { 
    $(document).on('submit', '#genbill', function()
    {  
              $('#loadicon').show();

      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'adm_pen_save.php',
        data : data,
        success: function(data) {  
                     $('#response').html(data);  
        // document.getElementById("hidemodal").click();  
        // window.alert(data);
        document.getElementById("genbill").reset();
            $('#loadicon').hide(); 

        $('#user_data').DataTable().ajax.reload();  
        }
      });
      return false;
    }); 
   }); 
</script>
<?php 
  include_once 'footer.php';
?>  
 