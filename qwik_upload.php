<?php require('header.php');

    // $qry1 = $conn_rrpl->query('select iupdate from billing_role where user="'.$getuid.'" and type="reports_view"');
    // $qry2 = $qry1->fetch_assoc();
    // $qry3 = $qry2['iupdate']; 
    // if ($qry3!='1'){

    //     echo "<script type='text/javascript'>
    //     alert('Access Denied - Please Contact Admin !');
    //     window.location.href='index.php';
    //     </script>";
    //     exit();

    // }



// date should be single day / yesterday only allowed 
// format should be excel only 
// largest to smallest / 00 to 23
// manual date formatting - yyyy-mm-dd hh:mm:ss (validation*) 
?>


 <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
    <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
 <style>
 

 #user_data_paginate{
  background-color: #fff;
 }
 button.dt-button, div.dt-button, a.dt-button{
  padding: 0.2em 1em;
 }
div.dt-button-collection {
    max-height: 300px;
    overflow-y: scroll;
}

 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
 </style>
<style type="text/css">
  .applyBtn{
    border-radius: 0px !important;
  }
  .show-calendar{
    top: 180px !important;
  } 
    .applyBtn{
        border-radius: 0px !important;
    }
    table.table-bordered.dataTable td {
        padding: 10px 5px 10px 10px;
    }
     .dt-buttons{float: right !important;}
    .user_data_filter{
        float: right;
    }

    .dt-button {
        padding: 5px 20px;
        text-transform: uppercase;
        font-size: 12px;
        text-align: center;
        cursor: pointer;
        outline: none;
        color: #fff;
        background-color: #37474f ;
        border: none;
        border-radius:  2px;
        box-shadow: 0 4px #999;
    }

    .dt-button:hover {background-color: #3e8e41}

    .dt-button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    #user_data_wrapper{
        width: 100% !important;
    }
    .dt-buttons{
        margin-bottom: 20px;
    }


#appenddiv, #appenddiv2 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444;} .component{display: none;} 
  table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
  .table .thead-light th{
    text-transform: none !important;
  } 
  label{
    text-transform: uppercase;
  }


  #appenddivcons, #appenddivfrom, #appenddivto, #appenddivship, #appenddivbill, #appenddivitem, #appenddivdo, #appenddivlr, #appenddivtno, #appenddivinv, #appenddivbillparty {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
</style>
<script type="text/javascript"> 

$(function() {
    $("#aftername").autocomplete({
    source: 'rate_party_auto.php',
    appendTo: '#appenddiv',
    select: function (event, ui) { 
               $('#aftername').val(ui.item.value);   
               $('#afterid').val(ui.item.dbid);      
               return false;
    },
    change: function (event, ui) {
    if(!ui.item){
        $(event.target).val("");
      Swal.fire({
      icon: 'error',
      title: 'Error !!!',
      text: 'Rate Model not assigned / Party does not exists !'
      })
      $("#afterid").val("");
      $("#aftername").val("");
      $("#aftername").focus();
    }
    }, 
    focus: function (event, ui){
    return false;
    }
    });
  });


</script>
 
   


 <div class="col-md-12"> <h3>Upload LR</h3> </div>
  
 <div id="response"></div>
  
 <div class="col-md-12" >
<div class="card-body "  style="background-color: #fff; border: 1px solid #ccc;">

<form id="upl52" action="" method="post" autocomplete="off"  enctype="multipart/form-data">
<div class="row">

 
 

<div class="col-md-4">
<div class="">
    <label for="">  LR EXCEL</label>
    <div class="row">
    <div class="col-md-12"> 
      <input name="file" type="file" id="file3" class="form-control" required="required" > 
      </div>   
    </div>
 </div> 
</div>
  <div class="col-md-2" style="margin-bottom: 25px; margin-top: 15px; ">
    <div class="row"> 
    <div class="col-md-12">
      <button style="" id="uplbtn3" type="submit" name="submit" class="btn btn-success" > UPLOAD
      </button> 
    </div>
    </div>  
  </div>
</div>
</form>

<div id="updateresponse">
  
</div>

<div class="card-body" >
        
        <div class="row">
                <div class="col-md-12" id="msg52">

                </div>
        </div>

</div>
<!--   <div class="row">
 
  

    <div class="col-md-1" style="top:-5px !important;"> 
      <label style=""> </label> <br>
      <button type="submit" class='btn btn-success'> <i class='fa fa-search '></i> <b>  </b> </button>    
      
    </div>
  </div> -->
</div> 
</div>

<div id="getPAGEDIV" class="col-md-12" style="margin-top: 25px !important;"></div>

 

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog modal-lg">  
           <div class="modal-content" id="employee_detail">  
           </div>  
      </div>  
 </div>   
<script>
 $(document).ready(function (e) {
    $('#loadicon').hide(); 
        $("#upl52").on('submit',(function(e) {
        e.preventDefault();
        $("#msg52").empty();
        $('#loadicon').show();  
        var form_data = new FormData(this);   
          
        $( "#uplbtn3" ).prop( "disabled", true );
            $.ajax({
                url: "qwik_upload_data.php",   
                type: "POST",           
                data: form_data,  
                contentType: false,    
                cache: false,            
                processData:false,       
                success: function(data)   
                    { 
                    $('#ip52').hide();
                    $("#upl52")[0].reset(); 
                    $('#loadicon').hide();
                    $("#msg52").html(data);
                    $( "#uplbtn3" ).prop( "disabled", false ); 
                    }
            });
        })); 
    }); 
</script>
 <?php include('footer.php'); ?>