<?php 

	require('connect.php');
 
    $report = $conn -> real_escape_string($_POST['seltype']);
    if(isset($_POST['veh'])){
   	 	$branch = $conn -> real_escape_string($_POST['veh']);
	} else {
		$branch = "";
	}
	if(isset($_POST['daterange'])){
    	$daterange = $conn -> real_escape_string($_POST['daterange']);
	} else {
		$daterange = "";
	}
	if(isset($_POST['bno'])){
    	$lrno = $conn -> real_escape_string($_POST['bno']);
	} else {
		$lrno = "";
	}
  
	function before ($thiss, $inthat)
	{
	    return substr($inthat, 0, strpos($inthat, $thiss));
	}

	function after ($thiss, $inthat)
	{
	    if (!is_bool(strpos($inthat, $thiss)))
	        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
	}
	  
$daterange = $_POST['daterange']; 

$fromdate = before('-', $daterange); 
$fromdate = date("Y-m-d", strtotime($fromdate)); 
$todate = after('-', $daterange); 
$todate = date("Y-m-d", strtotime($todate));
 
 
$date1=date_create($fromdate);
$date2=date_create($todate);
$diff = date_diff($date1,$date2);
$diff = $diff->format("%a");
if($diff>31){
  echo "<script> alert('Date difference can\'t be more than 31 days !'); </script>";
  exit();
}

$output ='';
 
 		if($branch=="ALL"){

		$result = mysqli_query($conn,"select l.truck_no, r.del_date,r.veh_type,l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rrpl_database.rcv_pod r LEFT join rrpl_database.lr_sample l on l.lrno = r.lrno where r.consignor_id='56' and (l.date BETWEEN '$fromdate' and '$todate') order by l.date, r.lrno");
        } else {
		
		$result = mysqli_query($conn,"select  l.truck_no, r.del_date,r.veh_type,l.consignee, r.bill_no, r.pod_date as poddate, r.id, r.bill_done, r.frno as fno, r.lrno as lrno, l.date as lrdate, l.branch as lrbranch, l.dest_zone as desti,l.item as item, r.branch as podbranch, r.pod_copy as upload from rrpl_database.rcv_pod r LEFT join rrpl_database.lr_sample l on l.lrno = r.lrno where r.consignor_id='56' and l.branch='$branch' and (l.date BETWEEN '$fromdate' and '$todate') order by l.date, r.lrno"); 
        }



if(mysqli_num_rows($result) == 0)
{
	echo "<script type='text/javascript'>
		alert('No result found !');
		window.location.href='qwik_down.php';
		</script>";
		exit();
}

 $output .= '
   <table border="1">  

            <th style=" text-align: center;  color:#444;"> Sno </th>
            <th style=" text-align: center;  color:#444;"> FM No </th> 
            <th style=" text-align: center;  color:#444;"> LR No </th> 
            <th style=" text-align: center;  color:#444;"> LR Date </th>
            <th style=" text-align: center;  color:#444;"> LR Branch </th>
            <th style=" text-align: center;  color:#444;"> Destination </th>
            <th style=" text-align: center;  color:#444;"> Material </th>
            <th style=" text-align: center;  color:#444;"> Truck No </th>
            <th style=" text-align: center;  color:#444;"> Consignee </th>
            <th style=" text-align: center;  color:#444;"> Unloading  </th>
            <th style=" text-align: center;  color:#444;"> POD Date </th>
            <th style=" text-align: center;  color:#444;"> POD Branch </th>
            <th style=" text-align: center;  color:#444;"> Bill Status </th> 
            <th style=" text-align: center;  color:#444;"> Upload </th> 

	</tr>
  ';
  $sno="0";
  while($row = mysqli_fetch_array($result))
  {
  $sno++;

  $pod_files1 = array(); 
  $copy_no = 0;
  foreach(explode(",",$row['upload']) as $pod_copies)
  {
  $copy_no++;
        if (strpos($pod_copies, 'pdf') !== false) {
        $file = 'PDF';
        } else {
        $file = 'IMAGE';
        }
  if($row['veh_type']=="MARKET"){
    $pod_files1[] = "<center><a href='https://rrpl.online/b5aY6EZzK52NA8F/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
   } else {
    $pod_files1[] = "<center><a href='https://rrpl.online/diary/close_trip/$pod_copies' target='_blank'>$file: $copy_no</a></center>";
   }
  }
  $podcopy = implode(", ",$pod_files1);

  $class = "";

if($row['bill_no']==""){
	$billno = "Not Billed";
} else {
	$billno = $row['bill_no'];
}
	// if ($row["ho_pod_check"] == "1" ){
	// $stat = "Approved";    
	// $class = "style='color: green !important; text-align: left;'";
	// }
	// else if ($row["ho_pod_check"] == "-1" ){
	// $stat = "Rejected";    
	// $class = "style='color: red !important; text-align: left;'";
	// } else {
	// $stat = "Pending";  
	// $class = "style='text-align: left;'";
	// }


   $output .= '
				<tr> 
							<td '.$class.'>'.$sno.'</td> 
							<td '.$class.'>'.$row["fno"].'</td> 
							<td '.$class.'>'.$row["lrno"].'</td> 
							<td '.$class.'>'.date('d/m/Y', strtotime($row['lrdate'])).'</td> 
							<td '.$class.'>'.$row["lrbranch"].'</td> 
							<td '.$class.'>'.$row["desti"].'</td> 
							<td '.$class.'>'.$row["item"].'</td>  
							<td '.$class.'>'.$row["truck_no"].'</td>  
							<td '.$class.'>'.$row["consignee"].'</td>  
							<td '.$class.'>'.date('d/m/Y', strtotime($row['del_date'])).'</td>  
							<td '.$class.'>'.date('d/m/Y', strtotime($row['poddate'])).'</td>  
							<td '.$class.'>'.$row["podbranch"].'</td>  
							<td '.$class.'>'.$billno.'</td>  
							<td '.$class.'>'.$podcopy.'</td>  

				</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  $name = "POD_".$fromdate."to".$todate.".xls";
  header('Content-Disposition: attachment; filename='.$name.'');
  echo $output;
  exit();