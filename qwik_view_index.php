<?php 
  include_once 'header.php';
?>  
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script>
  $( function() {  
      $( "#lrno" ).autocomplete({
 
              source: function(request, response) {  

                   $.ajax({
                    url: "adm_bill_fetch1.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                     search: request.term
                    },
                    success: function( data ) {
                      response( data );
                    }
                   }); 

              },
              select: function (event, ui) { 
                 $('#lrno').val(ui.item.value);   
                 return false;
              },
              appendTo: '#appenddiv2', 
              change: function (event, ui) {  
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                    alert('LR does not Exist !'); 
                  } 
              },  
      }); 
      }); 
</script> 
<script>
  $( function() {  
      $( "#billno" ).autocomplete({
 
              source: function(request, response) {  

                   $.ajax({
                    url: "adm_bill_fetch2.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                     search: request.term
                    },
                    success: function( data ) {
                      response( data );
                    }
                   }); 

              },
              select: function (event, ui) { 
                 $('#billno').val(ui.item.value);   
                 return false;
              },
              appendTo: '#appenddiv1', 
              change: function (event, ui) {  
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                    alert('Bill does not Exist !'); 
                  } 
              },  
      }); 
      }); 
</script> 
<style type="text/css">
  .dataTables_scroll{
    margin-bottom: 20px;
  }
  .dataTables_paginate{
    background: #fff;
  }
  table{
    margin: 0px !important;
  }
  .applyBtn{
    border-radius: 0px !important;
  }
  .show-calendar{
    top: 180px !important;
  } 
    .applyBtn{
        border-radius: 0px !important;
    }
    table.table-bordered.dataTable td {
        padding: 10px 5px 10px 10px;
    }
     .dt-buttons{float: right;}
    .user_data_filter{
        float: right;
    }

    .dt-button {
        padding: 5px 20px;
        text-transform: uppercase;
        font-size: 12px;
        text-align: center;
        cursor: pointer;
        outline: none;
        color: #fff;
        background-color: #37474f ;
        border: none;
        border-radius:  2px;
        box-shadow: 0 4px #999;
    }

    .dt-button:hover {background-color: #3e8e41}

    .dt-button:active {
        background-color: #3e8e41;
        box-shadow: 0 5px #666;
        transform: translateY(4px);
    }
    #user_data_wrapper{
        width: 100% !important;
    }
    .dt-buttons{
        margin-bottom: 20px;
    }


#appenddiv, #appenddiv2 {
    display: block; 
    position:relative
} 
.ui-autocomplete {
    position: absolute;
}
label{color: #000 !important;}
.dataTables_info{
  margin-top: 10px;
}
 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-right:10px; width: 250px; }.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop} .table .thead-light th{text-align: center; font-size: 11px; color:#444;} .component{display: none;} 
  table {width: 100% !important;} table.table-bordered.dataTable td { white-space: nowrap; overflow: hidden; text-overflow:ellipsis;  }
  .table .thead-light th{
    text-transform: none !important;
  }
</style>


<div id="response"></div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
</div>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">

<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 
 <div class="col-md-12"> <h3> POD BILL </h3> </div>

<div class="col-md-12" >
<div class="card shadow mb-4" style="border-radius: 0px !important;"> 
 
<div class="card-body" style="border: 1px solid #ccc;"> 

 
<form method="post" action="" id="getPAGE" autocomplete="off" enctype='multipart/form-data'> 
<div class="col-md-12" >
<div class="card-body "  style="background-color: #fff;">

  <div class="row">
 

    <div class="col-md-2">
      <label>LR/BILL</label>
      <select class="form-control" onChange="updt(this);"  required="required" name="type">
        <option value=""> -- select -- </option>
        <option value="LR"> LR NO </option>
        <option value="BILL"> BILL NO </option>
      </select>
    </div> 
 
     
    <div class="col-md-2">
      <label>LR No</label>
      <input type="text" name="lrno" value="" id="lrno" class="form-control" disabled="">
      <div id="appenddiv2"></div>

    </div> 

    <div class="col-md-2">
      <label>Bill No</label>
      <input type="text" name="billno" value="" id="billno" class="form-control" disabled="">
      <div id="appenddiv1"></div>
    </div> 
  
    <div class="col-md-1"> 
      <label> &nbsp; </label> 
      <button style="margin-top: -6px;" type="submit" class='btn btn-warning'> <i class='fa fa-search '></i> <b> &nbsp; SEARCH </b> </button>   
    </div>  

  </div>
</div>
</form> 
  <!-- <div id="getPAGEDIV" class="" style=" table-responsive">  -->
  
  <div id="getPAGEDIV" class="" style="margin-top: 20px;"> 


  </div>
</div>
</div>
</div> 
</div>
</div> 
</div>
</div>

<script type="text/javascript">
   function updt(sel) { 
      if(sel.value == "LR")
      {
        $("#lrno").prop('required',true);
        $("#lrno").prop('disabled',false);   
        $("#billno").prop('required',false);
        $("#billno").prop('disabled',true);  

      } else if(sel.value == "BILL") {
        $("#billno").prop('required',true);
        $("#billno").prop('disabled',false);   
        $("#lrno").prop('required',false);
        $("#lrno").prop('disabled',true); 

      } else { 
        $("#lrno").prop('required',false);
        $("#lrno").prop('disabled',true);  
        $("#billno").prop('required',false);
        $("#billno").prop('disabled',true);   
      }
    }

  $(document).on('submit', '#getPAGE', function()
    {   
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'qwik_view_get.php',
        data : data,
        success: function(data) {       
        $('#getPAGEDIV').html(data);  
        }
      });
      return false;  
  });

    function delete_req(val){
      var id = val;  
      if (confirm('Are You Sure to Remove ?')){ 
        $.ajax({
            type: 'POST',
            url: 'qwik_view_remove.php',
            data: {id: val},
            success: function(data){ 
            alert(data);
            $('#user_data').DataTable().ajax.reload();
            }
        });
      } else {

      }
  }

</script>

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content" id="employee_detail">   
           </div>  
      </div>  
</div>    
 
<?php 
  include_once 'footer.php';
?>  
 