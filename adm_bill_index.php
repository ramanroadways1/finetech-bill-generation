<?php 
  include_once 'header.php';
?>  
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>

<script>
  $( function() {  
      $( "#lrno" ).autocomplete({
 
              source: function(request, response) {  

                   $.ajax({
                    url: "adm_bill_fetch1.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                     search: request.term
                    },
                    success: function( data ) {
                      response( data );
                    }
                   }); 

              },
              select: function (event, ui) { 
                 $('#lrno').val(ui.item.value);   
                 return false;
              },
              appendTo: '#appenddiv2', 
              change: function (event, ui) {  
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                    alert('LR does not Exist !'); 
                  } 
              },  
      }); 
      }); 
</script> 
<script>
  $( function() {  
      $( "#billno" ).autocomplete({
 
              source: function(request, response) {  

                   $.ajax({
                    url: "adm_bill_fetch2.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                     search: request.term
                    },
                    success: function( data ) {
                      response( data );
                    }
                   }); 

              },
              select: function (event, ui) { 
                 $('#billno').val(ui.item.value);   
                 return false;
              },
              appendTo: '#appenddiv1', 
              change: function (event, ui) {  
                  if(!ui.item){
                  $(event.target).val(""); 
                  $(event.target).focus();
                    alert('Bill does not Exist !'); 
                  } 
              },  
      }); 
      }); 
</script> 
<style> 
.table-hover tbody tr:hover td,.table-hover tbody tr:hover th{background-color:#ffedda}.table td{vertical-align:middle!important;font-size:11px!important;color:#000;font-family:Verdana,Geneva,sans-serif;padding-top:4px;padding-right:4px;padding-bottom:4px;padding-left:10px}.table-bordered td{border:3px solid #e3e6f0}#user_data_info,#user_data_length{float:left}#user_data_filter,#user_data_paginate{float:right}.paginate_button{color:#000;float:left;padding:6px 12px;text-decoration:none;border:1px solid #ccc;cursor:pointer}.ellipsis{display:none}[type=search]{margin-left:10px}.ui-autocomplete{z-index:2150000000!important}.container input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{border-radius:2px;position:absolute;top:0;height:20px;width:20px;background-color:#fff;border:1px solid #000}.container:hover input~.checkmark{background-color:#fff}.container input:checked~.checkmark{background-color:#fff}.container input:disabled~.checkmark{background-color:#eaecf4}.checkmark:after{content:"";position:absolute;display:none}.container input:checked~.checkmark:after{display:block}.container .checkmark:after{left:6px;top:-1px;width:8px;height:16px;border:solid #000;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}button:disabled,button[disabled]{border:1px solid #333!important;color:#333!important;cursor:no-drop}
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
  padding: 8px;
}
#appenddiv2, #appenddiv1 {
    display: block; 
    position:relative
} 
</style>

<div id="response"></div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"> 
            <div class="dash" id="modaldetail"> 
            </div> 
        </div>
    </div>
</div>

<div id="content-wrapper" class="d-flex flex-column"> 
<div id="content">

<div id="updatereq_status"></div> 
 <div class="container-fluid"> 
<div class="row"> 

<div class="col-md-12" >
<div class="card shadow mb-4" style="border-radius: 0px !important;"> 
<div class="card-header">
 
<h6 class="m-0 font-weight-bold text-dark" style="top: 6px; float: left !important; font-family: Verdana, Geneva, sans-serif; font-weight: normal; text-transform: uppercase;"> VIEW POD/BILL  </h6> 
</div>
<div class="card-body" style="margin-top: 10px;"> 

 
<form method="post" action="" id="getPAGE" autocomplete="off" enctype='multipart/form-data'> 
<div class="col-md-12" >
<div class="card-body "  style="background-color: #fff; border: 1px solid #ccc;">

  <div class="row">
 

    <div class="col-md-2">
      <label>LR/BILL</label>
      <select class="form-control" onChange="updt(this);"  required="required" name="type">
        <option value=""> -- select -- </option>
        <option value="LR"> LR NO </option>
        <option value="BILL"> BILL NO </option>
      </select>
    </div> 
 
     
    <div class="col-md-2">
      <label>LR No</label>
      <input type="text" name="lrno" value="" id="lrno" class="form-control" disabled="">
      <div id="appenddiv2"></div>

    </div> 

    <div class="col-md-2">
      <label>Bill No</label>
      <input type="text" name="billno" value="" id="billno" class="form-control" disabled="">
      <div id="appenddiv1"></div>
    </div> 
  
    <div class="col-md-1"> 
      <label> &nbsp; </label> 
      <button style="margin-top: -6px;" type="submit" class='btn btn-warning'> <i class='fa fa-search '></i> <b> &nbsp; SEARCH </b> </button>   
    </div>  

  </div>
</div>
</form> 
  <!-- <div id="getPAGEDIV" class="" style=" table-responsive">  -->
  
  <div id="getPAGEDIV" class="" style="margin-top: 20px;"> 


  </div>
</div>
</div>
</div> 
</div>
</div> 
</div>
</div>

<script type="text/javascript">
   function updt(sel) { 
      if(sel.value == "LR")
      {
        $("#lrno").prop('required',true);
        $("#lrno").prop('disabled',false);   
        $("#billno").prop('required',false);
        $("#billno").prop('disabled',true);  

      } else if(sel.value == "BILL") {
        $("#billno").prop('required',true);
        $("#billno").prop('disabled',false);   
        $("#lrno").prop('required',false);
        $("#lrno").prop('disabled',true); 

      } else { 
        $("#lrno").prop('required',false);
        $("#lrno").prop('disabled',true);  
        $("#billno").prop('required',false);
        $("#billno").prop('disabled',true);   
      }
    }

  $(document).on('submit', '#getPAGE', function()
    {   
      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'adm_bill_get.php',
        data : data,
        success: function(data) {       
        $('#getPAGEDIV').html(data);  
        }
      });
      return false;  
  });
</script>

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content" id="employee_detail">   
           </div>  
      </div>  
</div>    





<!-- <script type="text/javascript">
jQuery( document ).ready(function() {

$('#loadicon').show(); 
var table = jQuery('#user_data').dataTable({
     "lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
     "bProcessing": true,
     "sAjaxSource": "adm_pen_fetch.php",
      "bPaginate": true,
      "sPaginationType":"full_numbers",
      "iDisplayLength": 10,
      //"order": [[ 8, "desc" ]],
      "columnDefs":[
      {
        "targets":[0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        "orderable":false,
      },
      ],
      "aoColumns": [
        { mData: '0' },
        { mData: '1' } ,
        { mData: '2' },
        { mData: '3' },
        { mData: '4' },
        { mData: '5' },
        { mData: '6' },
        { mData: '7' },
        { mData: '8' },
        { mData: '9' }
    ],
    "initComplete": function( settings, json ) {
    $('#loadicon').hide();
    }
});  
   
}); 

  function action(val,stat){
        $('#loadicon').show();
        $.ajax({
            type: 'POST',
            url: 'adm_pen_action.php',
            data: {id: val, act: stat},
            success: function(data){ 
            // alert(data);
            $('#user_data').DataTable().ajax.reload();
            $('#loadicon').hide(); 
            }
        }); 
  }

</script>
<script type="text/javascript">
$(document).ready(function() { 
    var table = $('#user_data').DataTable(); 
} );
</script>  

<script> 
  $(document).ready(function()
  { 
    $(document).on('submit', '#genbill', function()
    {  
              $('#loadicon').show();

      var data = $(this).serialize(); 
      $.ajax({  
        type : 'POST',
        url  : 'adm_pen_save.php',
        data : data,
        success: function(data) {  
                     $('#response').html(data);  
        // document.getElementById("hidemodal").click();  
        // window.alert(data);
        document.getElementById("genbill").reset();
            $('#loadicon').hide(); 

        $('#user_data').DataTable().ajax.reload();  
        }
      });
      return false;
    }); 
   }); 
</script> -->
<?php 
  include_once 'footer.php';
?>  
 