<?php
 
require('connect.php');
ini_set('max_execution_time', '300');
require_once('./vendor/php-excel-reader/excel_reader2.php');
require_once('./vendor/SpreadsheetReader.php');

// if(isset($_POST['daterange']) && isset($_POST['billparty'])){

// function before ($thiss, $inthat)
// 	{
// 	    return substr($inthat, 0, strpos($inthat, $thiss));
// 	}

// 	function after ($thiss, $inthat)
// 	{
// 	    if (!is_bool(strpos($inthat, $thiss)))
// 	        return substr($inthat, strpos($inthat,$thiss)+strlen($thiss));
// 	}

// $daterange = $_POST['daterange'];
// $billparty = $_POST['billparty'];
  
// 	 $fromdate = before('-', $daterange); 
// 	 $fromdate = date("Y-m-d", strtotime($fromdate)); 
// 	 $todate = after('-', $daterange); 
// 	 $todate = date("Y-m-d", strtotime($todate));
 
//  		$sql = "SELECT rrpl_database.rate_model.* FROM rrpl_database.billing_party left join rrpl_database.rate_model on rrpl_database.rate_model.id = rrpl_database.billing_party.modeltype where rrpl_database.billing_party.id='$billparty'"; 
// 		$res = $conn->query($sql);
// 		$upl = $res->fetch_assoc();	

$uploadid = strtoupper(chr(rand(65, 90)) . strtotime('now') . chr(rand(97,122)));

$allowedFile = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel'];

if(in_array($_FILES["file"]["type"],$allowedFile)){

$targetPath =  './uploads/'.$uploadid.$_FILES['file']['name']; 

if(move_uploaded_file($_FILES['file']['tmp_name'], $targetPath)){

$Reader = new SpreadsheetReader($targetPath);
$sheetCount = count($Reader->sheets());
 

$lineno = 0;
$exceldate="";
try {
$conn->query("START TRANSACTION"); 
for($i=0;$i<$sheetCount;$i++){

$Reader->ChangeSheet($i);
 
	foreach ($Reader as $line) {
 
	@$lrno  = strtoupper($line[0]);
	@$billno  = strtoupper($line[1]);
	// @$tstation  = strtoupper($line[1]);
	// @$trucktype  = strtoupper($line[2]);
	// @$pertonerate = $line[3]; 
	// @$fixedrate = $line[4]; 
	  					
	  				if($lineno>0){
	  					
						// if($upl['pertone']=="0" && $pertonerate!=""){
						//  	throw new Exception("Error: PerToneRate not allowed !");  
						// 		}
						// 		if($upl['fixedrate']=="0" && $fixedrate!=""){
						//  	throw new Exception("Error: FixedRate not allowed !");  
						// 		}
						// 		if($upl['trucktype']=="0" && $trucktype!=""){
						//  	throw new Exception("Error: TruckType not allowed !");  
						// 		}
	  						$rowno = $lineno+1;
							if($billno==""){
						 		throw new Exception("Error: Bill not exist on line $rowno !");  
							}

	  					$sqlchk = "insert into rrpl_database.finetech_lr (lrno, billno, uploadid) values ('$lrno', '$billno', '$uploadid')";
						if ($conn->query($sqlchk) === FALSE) {
						 	throw new Exception("Error: Something Went Wrong !");  
						}
	  				}	

	$lineno = $lineno + 1;
	}
}
$conn->query("COMMIT");
 
			echo ' 
		  	<table id="user_data" class="table table-bordered table-hover" style="background-color:#fff;">
		      <thead class="thead-light">
		        <tr>
					<th>LR No</th>
					<th>Bill No</th>
					<th>LR Date</th>
					<th>LR Branch</th>
					<th>Destination</th>
					<th>Item</th> 
					<th>Consignee</th> 
					<th>Unloading</th> 
					<th>Bill Status</th> 
					<th>POD Date</th> 
					<th>POD Branch</th> 
					<th>Upload</th> 
					<th>Status</th> 
				</tr>
		      </thead> 
		 	</table> 
 
		<script type="text/javascript">
		jQuery( document ).ready(function() {

		$("#loadicon").show(); 
		var table = jQuery("#user_data").dataTable({
		"lengthMenu": [ [10, 500, 1000, -1], [10, 500, 1000, "All"] ], 
		"bProcessing": true,
  		"searchDelay": 1000,

        "scrollY": 400,
        "scrollX": true,
		"sAjaxSource": "qwik_upload_table.php?p='.$uploadid.'",
		"bPaginate": true,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 10,
		"ordering": false, 
		"language": {
            "loadingRecords": "&nbsp;",
            "processing": "<center> <font color=brown> Please wait while Loading </font> <img src=https://www.mypsdtohtml.com/_ui/images/loading.gif height=20> </center>"
        },
		dom: "<\'toolbar\'>lBfrtip",
		      buttons: [ 
		     	 {
extend: "csv",
		      	},
		         
		          {
		              text: "UPDATE BILL NO",
		              action: function ( e, dt, node, config ) {
					if (confirm("Are you sure you want to UPDATE ?")) {
								UpdateRate();
					} else {
						alert("error");
					}
		              }
		          }
		      ],
		"initComplete": function( settings, json ) {
		$("#loadicon").hide();
		}
		});  
 

        $("div.toolbar").html(\'<select class="form-control" style="width: 180px; max-height:32px; float:right;" id="table-filter"><option value=""> All LR </option><option value="Invalid"> Invalid LR </option> </select>\');


		}); 
     
		$(document).ready(function() { 
		var table = $("#user_data").DataTable(); 
		$("#table-filter").on("change", function(){
		table.search(this.value).draw();   
		});
		} ); 


		function UpdateRate(){
		$("#loadicon").show(); 
		$.ajax({  
		    url:"qwik_upload_save.php?id='.$uploadid.'",  
		    method:"post",  
		    success:function(data){ 
				  $("#updateresponse").html(data); 
		          // $("#user_data").DataTable().ajax.reload(); 
		          $("#loadicon").hide(); 
		    }  
		});
		}

		 
		</script>';
}
catch(Exception $e) { 
$conn->query("ROLLBACK"); 
echo '<div class="alert alert-danger"><strong> '.$e->getMessage().' </strong></div>' ;
}            
} else {
echo '<div class="alert alert-danger"><strong> Uploading Error ! </strong></div>' ;
}
} else {
echo '<div class="alert alert-danger"><strong> Invalid File !</strong></div>' ; 
}
// @unlink($targetPath);   
// }

// $files = glob('../uploads/*');
// foreach($files as $file){
//   	if(is_file($file))
//     @unlink($file);
// }

mysqli_close($conn); 
?> 